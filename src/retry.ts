import { delay } from './delay';

interface RetryPromiseOptions<T> {
	promise: Promise<T>;
	nthTry: number;
	delayTime?: number;
}

export async function retryPromise<T>(payload: RetryPromiseOptions<T>) {
	const { promise, nthTry, delayTime = 1000 } = payload;
	try {
		// try to resolve the promise
		const data = await promise;
		// if resolved simply return the result back to the caller
		return data;
	} catch (e) {
		if (nthTry === 1) {
			return Promise.reject(e);
		}
		// if the promise fails and the current try is not equal to 1
		// we call this function again from itself but this time
		// we reduce the no. of tries by one
		// so that eventually we reach to "1 try left" where we know we have to stop and reject
		await delay(delayTime);
		return retryPromise({ promise, nthTry: nthTry - 1, delayTime });
	}
}
